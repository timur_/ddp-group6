# __Design Driven Project - Group 6 - Final Documentation__
## __Personal air quality detection and alarming system__
### Timur Kuzu, Yifan Cheng 

 ## Introduction
 This project uses a Raspberry Pi 4 to run an attached [SGP30](https://www.sparkfun.com/products/16531) and [PMSA003I](https://www.adafruit.com/product/4632) sensor, able to record TVOC, equivalent CO<sub>2</sub> and particulate matter levels.  
 The data is then displayed in a dashboard and warnings (LED and buzzer) are given using Node-Red.

 ## Hardware
 The following hardware is used:
 - [Raspberry Pi 4 Mosel b](https://www.raspberrypi.org/products/raspberry-pi-4-model-b/)
 - [SparkFun Air Quality Sensor - SGP30 (Qwiic)](https://www.sparkfun.com/products/16531)
 - [Adafruit PMSA003I Air Quality Breakout](https://www.adafruit.com/product/4632)
 - buzzer
 - LED light
 - 270 Ω resistor
 - Jumper Wires


 ## Software
 - [Node-Red](https://nodered.org/)
 - [Pimoroni Python library](https://github.com/pimoroni/sgp30-python)
 - [Adafruit CircuitPython](https://github.com/adafruit/circuitpython)

 ## Instructions
 ### Step 1: connect Raspberry Pi with SGP30 and PMSA003

 Since both SGP30 and PMSA003 communicate via I2C and the Raspberry Pi 4 has only one pair(SDA,SCL) of I2C pins, we can use a breadboard to connect the two sensors to a single pair of I2C pins.  
 The four connections needed are:
 
 |SGP30     |Raspberry Pi       |PMSA001   |
 | :-----   | :----             | :----    |
 |3.3v      |3v3 power (pin 1)  |3V        | 
 |SDA       |GPIO2 SDA (pin 3)  |SDA       |
 |SCL       |GPIO2 SCL (pin 5)  |SCL       |
 |GND       |Ground    (pin 6)  |GND       |
 
 ![Wiring Diagram](/media/wiring%20diagram.jpg )
 
 Then detect all I2c addresses,using the following command:
 ```
 i2cdetect -y 1
 ```
 or check it by using this flow in Node-Red:  
 ![check i2c adresses in node-red](/media/i2cdetect1.png  )  
   
 If the two of your I2C devices use different addresses like the following picture shows, congratulations you are done with it.  
 But if the two I2C devices share the same address, please [follow this link](https://www.instructables.com/Raspberry-PI-Multiple-I2c-Devices/) to help you fix it.  
 ![result of i2c adresses](/media/i2cdetect2.png)

 ### Step 2: connect LED light and buzzer to Raspberry Pi
 You need an LED, a 270Ω resistor and jumper wires to light an LED with Raspberry Pi's GPIO Pins.  
 The longer leg of the LED light(anode) is connected to the positive supply of circuit, the power will be provided by a GPIO pin, we use pin 18 here.  
 The shorter leg(cathode) should be connected to negative side of the power supply, know as 'ground'.  
 To prevent the circuit current from burning out the LED light, you __must__ use resistors to connect LEDs up to the GPIO pins of the Raspberry Pi.  
 A 270Ω resistor will be used.([Click here for a resistor color code calculator](https://www.calculator.net/resistor-calculator.html?bandnum=4&band1=green&band2=red&band3=blue&multiplier=blue&tolerance=gold&temperatureCoefficient=brown&type=c&x=67&y=21))   
 Your circuit should look like this:  
 ![LED wiring diagram](/media/LEDwiringdiagram.png )  
 Next, check the positive and negative leg of the buzzer, connect the positive one to a GPIO pin and the negative one to the ground.
 ![buzzer wiring diagram](/media/buzzer%20wiring%20diagram.png )
 ### Step 3: software setup
 Raspberry Pi needs to be updated and patched to the latest version before use:
 ```
 sudo apt-get update
 sudo apt-get upgrade
 ```
 Install the [Python library](https://github.com/pimoroni/sgp30-python) for SGP30 air quality sensor.
 ```
 git clone https://github.com/pimoroni/sgp30-python
 cd sgp30-python
 sudo ./install.sh
 ```
 Install [Adafruit CircuitPython](https://github.com/adafruit/circuitpython).  
 Install CircuitPython library for PM2.5 Air Quality Sensors.
 ```
 pip3 install adafruit-circuitpython-pm25
 ```
 To install system-wide (this may be required in some cases):
 ```
 sudo pip3 install adafruit-circuitpython-pm25
 ```
 ### Step 4: running on Node-Red
 Now that all the preparations are done, open Node-Red to run our air quality monitoring. First of all, clone this repo to your Node-Red projects.  
 Run Node-Red on your Raspberry Pi:  
 ![open node-red](/media/open%20node-red.png)  
 Copy the HTTPS link of this repo.  
 ![copy the link](/media/copy%20the%20link.png)    
 Click the hamburger button in the upper right corner and then choose 'open projects'.  
 ![open project](/media/open%20project.png)  
 If you don't have this projects button, please find this __.node-red__ folder in the root directory of your computer, then open this __setting.js__ file, find the place where the project settings are located and change the line 'enabled: false' to 'enabled: true'.  
 ![node-red folder](/media/node-red%20folder.png) ![setting file](/media/node-red%20setting%20file.png) ![active project](/media/active%20project.png)  
 Paste the HTTPS link into the red box.  
 ![clone repo](/media/clone%20repo.png)  
 You will see a page with many unknown nodes, at this point you will recieve a hint that some node types are missing, click the button 'Manage project dependencies' and install all missing node types.
 ![manage dependencies](/media/missing%20node%20types.png)![install node types](/media/install%20node%20types.png)
 Then you can see the following interface:  
  ![node-red interface](/media/node-red%20interface.png)   
 Now, let's run this air quality monitor. Open the dashboard first, click on the button as shown below.  
 ![open dashboad1](/media/open%20dashboard1.png)![open dashboard2](/media/open%20dashboard2.png)

 Click the __SGP30 python script__ button and __PMSA003 python script__ button on the left.  
 ![run SGP30](/media/run%20SGP30.png) ![run PMSA003I](/media/run%20PMSA003I.png)  
 Now you can see how the CO<sub>2</sub>/TVOC/PM1.0/PM2.5/PM10 values change over two minutes and the real-time guage of CO<sub>2</sub> and PM2.5 in the dashboard.  
 ![dashboard](/media/dashboard.png)

## Prototyping documentation
During our prototpying we came across various challenges which often required different approaches to be solved.  
This chapter deals with the prototyping efforts that were used within this project, also including the directions that did not turn out to be successful.

### SGP30 air quality sensor
With the goal to get the data of the SGP30 sensor into Node-Red, we first have to connect the sensor to the Raspberry Pi.  
For that, [Sparkfun's Qwiic connect system](https://www.sparkfun.com/qwiic) uses a 4-pin JST connector for fast development board integration and the ability to daisy-chain I2C devices.  
The 4 pins are used for power, ground, serial data (SDA) and serial clock (SCL) and connect to the General Purpose Input/Output (GPIO) header of the Raspberry Pi.  
First we had to know the cable layout of the Qwiic cables, which we found on the [Sparkfun Qwiic website](https://www.sparkfun.com/qwiic).  
![qwiic_layout](/media/qwiic_layout.jpg)  
To connect the sensor to the Raspberry Pi 4, we soldered female connectors to one end of the Qwiic cables.  
Afterwards, we used the pin-layout of the GPIO-header to connect the 3V3 power(pin1), ground(pin6), SDA(pin3) and SCL(pin5) pins accordingly.
![gpio_layout](/media/gpio_layout.png)  
Knowing which pins we had to use, we could now connect the 4 female Qwiic cables to the respective pins on the GPIO header.  
Doing so, a red light on the sensor is activated, indicating that power is available to the sensor.
![sgp30](/media/sgp30_1.jpg)  
Having worked with I2C sensors on a Raspberry Pi before, we were aware that we first need to enable the I2C interface on the Raspberry Pi 4 before taking any further steps.  
To do so, we simply followed an instruction from a Raspberry Pi forum: [Enable I2C Interface on the Raspberry Pi](https://www.raspberrypi-spy.co.uk/2014/11/enabling-the-i2c-interface-on-the-raspberry-pi/)  
Now we wanted to check if the sensor is actually recognized by the Raspberry Pi, so we found [this neat way](https://learn.adafruit.com/adafruits-raspberry-pi-lesson-4-gpio-setup/configuring-i2c) to check the I2C addresses and see which ones are currently in use.  
So we open the terminal and check for I2c devices using:  
 ```
 i2cdetect -y 1
 ```
The output responds that there is an active device running on address 58, which according to the sensor datasheet is exactly the address that the SGP30 sensor is running on.  
To make our lives easier for later, we also decided to implement this command within Node-Red early on, so that we don't have to open the terminal and run the command manually when we want to check for connected I2C devices later.  
This can be achieved easily by using an exec node to run said terminal command and displaying the output in the debug window.  
With Node-Red shipping preinstalled on the Raspberry Pi 4, we can just open the terminal and execute
 ```
 node-red
 ```  

to host a local server for our flows at http://127.0.0.1:1880.
![i2cdetect](/media/i2cdetect1.png)  
Implementing this simple flow then allows us to check for connected I2C devices by inserting a timestamp over the inject node.  
The output of said flow then looks like the following:  
![i2cdetect2](/media/i2cdetect2.png)  
Our next goal was then to get the sensor data on the Raspberry Pi.  
The first approach here was to do this in Node-Red directly.  
We did already integrate sensors into Node-Red in the DDP course before, so this was our first idea here as well.  
For that, we first installed the required I2C nodes via npm.
 ```
 npm install node-red-contrib-i2c
 ```
The new "i2c in" node should then allow us to read sensor data directly, given the I2C address and a command it's requested to read from.  
Based on this we added the I2C address (58) and tried different commands according to the sensor datasheet.  
However we were not able to read any data from the sensor like that.  
After consulting our supervisor, he hinted us that the i2c nodes only support 8bit addressing, whereas the sensor datasheet requires 16bit.  
With that in mind, we started looking for other approaches instead.

We then came across [this dedicated SGP30 node](https://flows.nodered.org/node/node-red-contrib-sgp30), which we also installed via npm using:
 ```
 npm install node-red-contrib-sgp30
 ```
As the example provided suggests, using the I2C address "58" and I2C bus set to "1" should give us JSON-object outputs looking similar to this:  

![sgp30node](/media/sgp30node.png)  
As the documentation of that node states, the package is based on this [Breakout Gardener library](https://github.com/xoblite/BreakoutGardener).  
For us however, using this node did not generate us any successful outputs.  
We suspect that this might be because our SGP30 sensor was made by SparkFun, however in that project a [Breakout version from Pimoroni](https://shop.pimoroni.com/products/sgp30-air-quality-sensor-breakout) was used instead.  
Maybe this let to incompatibility between the two sensors.  

Realizing that, we now shifted our approach and tried to read data from that sensor outside of Node-Red first, and  get that data into Node-Red later on.  
So we started looking for repos, that allow us to read the SGP30 sensor data and found [this repo by Pimoroni](https://github.com/pimoroni/sgp30-python).  
We then cloned that repo and installed the dependencies on the Raspberry Pi:
 ```
 git clone https://github.com/pimoroni/sgp30-python  
 cd sgp30-python
 sudo ./install.sh
 ```
Then we could open the examples folder inside the repo, open the terminal there and execute:
 ```
 python test.py
 ``` 
After the sensor warms up for 15 seconds, we get an update on the VOC value (parts per billion) and a CO2 equivalent (parts per million) every second. 

Our next step was then to integrate this data into Node-Red.  
For that, we did some research on how to run python scripts in Node-Red, and found [this node](https://flows.nodered.org/node/node-red-contrib-pythonshell).  
This allows us to set the path to a local python file, which can then be executed within Node-Red.  
Doing so we were then able to get the sensor data from the output-side of the pythonshell node.
![python_node](/media/python_node_sgp30.png)  
With that step completed, we now have the SparkFun SGP30 sensor data within Node-Red.  

### PMSA003I particulate matter sensor
To connect the PMSA003I sensor to the Raspberry Pi, we again require a 3v3 power, ground, SDA and SCL pin on the GPIO header.  
However with the SGP30 sensor already using the only SDA and SCL pins on the header, we needed to come up with a new solution for that.  
We then decided to use a breadboard instead, to connect both of the sensors to the breadboard, and then the breadboard to the Raspberry Pi.  
The I2C bus system then allows us to run both sensors over only one SDA and SCL pin.  
While we were at it, we also hooked up the 3v3 power and ground pins to the breadboard, to have a cleaner overall setup with less pins on the Raspberry Pi in use.  
To gain a basic understanding of how to use a breadboard in the first place, we started to watch some [tutorials on youtube](https://www.youtube.com/watch?v=6WReFkfrUIk).  
With the breadboard and the PMSA003I sensor connected, the new setup then looked like this:  
![setup01](/media/setup01.jpg)  

With the working software approach of the SGP30 sensor in mind, we decided to try the same with the PMSA003I sensor as well.  
So first step was again to find a working python repo and then integrate that into Node-Red.  
Fortunatly for us, Adafruit offers a [dedicated python repo](https://github.com/adafruit/Adafruit_CircuitPython_PM25) for their particulate matter sensors themselves.  
This repo is based on CircuitPython, a programming language similar to python and designed for simple microcontroller experimenting.  
Apart from microcontrollers however, there is also support for single board computers like the Raspberry Pi.  
So to get going with the repo, we first clone it again, then follow the instructions to install dependencies for running on a Raspberry Pi. 
 ```
 git clone https://github.com/adafruit/Adafruit_CircuitPython_PM25.git
 pip3 install adafruit-circuitpython-pm25
 ``` 
We can then just run the included example file to read sensor data from the particulate matter sensor.  
To run the example python script, we again navigate to the cloned repo, then into the examples folder, open the terminal here and execute:
 ```
python pm25_simpletest.py
 ``` 
With that up and running, we can again follow the same approach to get this data into Node-Red.  
That means we add another pythonshell node and configure it to run the pm25_simpletest.py file from the cloned particulate matter sensor repo.  
![python_node](/media/python_node_pmsa003i.png)  
Now we have successfully integrated the SGP30 and PMSA003I sensor data into Node-Red! 
### LED and buzzer sound
To create a visual and audio signal in case of high pollution values, the next steps are to integrate the LED and a buzzer to the breadboard.  
For the LED hardware connection, we referred to [this guide](https://thepihut.com/blogs/raspberry-pi-tutorials/27968772-turning-on-an-led-with-your-raspberry-pis-gpio-pins), explaining that we first need a resistor to limit the current coming from the GPIO pins.  
With a 270Ω resistor, we could now attach the LED to an open GPIO and ground pin on the Raspberry Pi.  
To know how much resistance the different resistors provide, we reffered [this color code calculator](https://www.calculator.net/resistor-calculator.html?bandnum=4&band1=green&band2=red&band3=blue&multiplier=blue&tolerance=gold&temperatureCoefficient=brown&type=c&x=67&y=21).  
Apart from that, we also had to make sure to maintain the right polarity of the LED, which means connecting the longer leg to power (GPIO pin) and the shorter leg to ground (ground pin).  
The final circuit setup was also provided by the guide and looked like this:  
![LEDwiring](media/LEDwiringdiagram.png)  
To control the LED over the GPIO pin in Node-Red, [this youtube video](https://www.youtube.com/watch?v=ibe1Ej2Tv7k) explained that we can simply use a GPIO-out node, select the pin that the power of the LED is connected to (pin18) and provide a digital output to that pin.  
We then also initialised the base-state of the pin to low, so that the LED is not powered by default.  
![LEDgpio](media/led-gpio.png)  
When now adding a dashboard switch as input to the GPIO-out node, we can open the dashboard, flip the switch and the LED on the breadboard lights up.

With the LED running, we could now work on getting the buzzer controlled over Node-Red as well.  

Before deciding to use a buzzer however, we tried using a [play audio node](https://flows.nodered.org/node/node-red-contrib-play-sound) that allows to play local sound files from the Raspberry Pi.   
So we downloaded an alarm sound on the Raspberry Pi in .wav-format and attached the file path to the play audio node.  
After not successfully playing any sounds, we tried [changing the file format to mp3](https://forums.raspberrypi.com/viewtopic.php?t=212527) using this command from FFmpeg:  
 ```
ffmpeg -i alarm.wav newalarm.mp3
 ``` 
We could then play the sound in Node-Red, however we still dropped this approach later because using a buzzer seemed to be a nicer solution here.  

The basic idea to connect the buzzer is the same as with the LED.  
To connect the buzzer to the breadboard, [this Raspberry Pi guide](https://projects.raspberrypi.org/en/projects/physical-computing/8) offers a nice image showing we again only need to wire the positive side of the buzzer to an available GPIO pin, and the negative to a ground pin.  
![buzzerwiring](media/buzzer-circuit.png)  
Software-wise, we used [this youtube guide](https://www.youtube.com/watch?v=b_lK61k0vko) to understand that only a GPIO-out node was needed to get a sound from the buzzer.  
So we select the GPIO pin connected to the buzzer (pin 37), this time however with a PWM signal.  
With using the GPIO-out node in PWM mode, we now need to inject numbers from 0 - 100 to control the state of the buzzer.  
Surprisingly, when inserting the number 100, the buzzer did not play any sound, when we changed that to 99 however, we did get sound.  
That's why we decided to set the input values to 99 when addressing the buzzer.  
![buzzerpgio](media/buzzer-gpio.png)  
By changing the frequency we can also adjust the height of the sound, after testing some frequencies from 100Hz-1000Hz we decided to stick with 300-500Hz.

### Bringing everything together in Node-Red
Now it was time to combine everything in Node-Red and implement the logic behind the flows, so that we get an active LED and buzzer when exceeding air pollution thresholds.  

At this point, we have the standard outputs according to the repos of the SGP30 and PMSA003I sensor, so first we have to separate the outputs to only get the data values for further operations.  
For the SGP30 sensor, the current output has the following format:  
![sgp30output](media/sgp30output.png)  
If we now only want to have the VOC data for example, our idea was to select everything from the output string that's between "VOC:" and "(ppb)".  
To implement this in Node-Red, we used this [string node](https://flows.nodered.org/node/node-red-contrib-string) to change the output string.  
Given a msg.payload, which we get from our pythonshell nodes, this allows us to manipulate the string to only keep information between "VOC:" and "(ppb)".  
In the following example, only the VOC or CO2 value is extracted without the additional output information.  
![dataextraction](media/data_extraction.png)  
![stringnode](media/stringnode.png)  
Using the same concept also allows us to get the other data values that we want to extract from the python outputs.  

As our msg.payload sensor data is currently still a string, we now need to change that into a number, so we can use that for mathematical operations after that.  
We came across this [Node-Red forum entry](https://discourse.nodered.org/t/string-to-number/988/7), explaining exactly how to do that with a function node and some JavaScript.  
![functionflow](media/functionflow.png)  
![functionnode](media/functioncode.png)  

With the single data values now available, we can also create our dashboard to display the pollution changes in charts.  
To do so, we can just add dashboard-chart and gauge nodes to our data and configure them to show data from the last 2 minutes.  
![dashboardearly](media/dashboardearly.png)  

After that, we started to look for safety thresholds to ensure healthy environments and compare the sensor data to:  
- [VOC](https://bridex.fujielectric.com/indoor-air-quality-iaq-sensor-co2-volatile-organic-compounds-voc): 450 ppb
- [CO2](https://www.kane.co.uk/knowledge-centre/what-are-safe-levels-of-co-and-co2-in-rooms): 700 ppm
- [PM2.5](https://www.indoorairhygiene.org/pm2-5-explained/): 35 µg/m3
- [PM10](https://learn.kaiterra.com/en/air-academy/pm10-particulate-matter-pollutes-air-quality):  50 µg/m3

To compare the data to these thresholds, we used switch nodes with the respective maximum allowed values.  
In case of exceeding the threshold, output 1 will be used, otherwise output 2.  
![switchflow](media/switchflow.png)  
![switchnode](media/switch.png)  
In case of CO2 >= 700 ppm, the following values will be changed:  
- msg.payload = 99, forwarded to buzzer
- msg.payload = 1, forwarded to LED   

In case of CO2 < 700 ppm, these values will be changed:  
- msg.payload = 0, forwarded to buzzer
- msg.payload = 0, forwarded to LED  

Transferring this to all the thresholds we wanted to observe (VOC,CO2,PM2.5,PM10) results in the following flow layout and dashboard:  
![totalflow](media/node-red%20interface.png)  
![totaldashboard](media/dashboardend.png)  

small problem fix for particulate matter data:  
As you might see in the dashboard charts, the particulate matter sensor sometimes gave us some random 0 values. 
To make the graphs more appealing we decided to delete these values using a switch node that only allows values > 0 to pass.

Here is also a video of testing the final prototype, using some disinfectant spray to trigger the air quality (VOC) sensor. (click videotest link below)  
![videotest](media/videotest.mp4)  

